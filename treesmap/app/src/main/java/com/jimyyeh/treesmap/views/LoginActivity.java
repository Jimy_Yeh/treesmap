package com.jimyyeh.treesmap.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jimyyeh.treesmap.R;
import com.jimyyeh.treesmap.models.TreesMapUser;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A login screen that offers login via username/password. And a Google Sign in
 *
 */
public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
//    private boolean isAdmin = false;

    // UI references.
    private AutoCompleteTextView mUsernameView;
    private EditText mPasswordView;
    private View mLoginFormView;
    public boolean isAdmin;
    private TreesMapUser globalUser;
    public RequestQueue queue;
    public String gUsername;
    public String gPassword;

    private static final String USER_DB = "http://10.0.2.2:1337/treesmapuser";
    private static final String URL_FIND_USER_BY_USERNAME = "/finduserbyusername?username=";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        findViewById(R.id.txtDisclaimer).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle(getString(R.string.login_warning_title))
                        .setMessage(R.string.login_warning)
                        .setPositiveButton("OK", null)
                        .show();
            }
        });

        // Set up the triggerAutoLogin form.
        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                validateLoginForm();
                return true;
            }
            return false;
            }
        });

        Button signInButton = (Button) findViewById(R.id.email_sign_in_button);
        signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                validateLoginForm();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);


        queue = Volley.newRequestQueue(this);
    }


    private void validateLoginForm() {
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the triggerAutoLogin attempt.
        gUsername = mUsernameView.getText().toString();
        gPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(gPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(gUsername)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }


        if (cancel) {

            focusView.requestFocus();
        } else {

//            UserDBHandler db2 = new UserDBHandler(this);
//            TreesMapUser u0 = new TreesMapUser("None", "q", "q", 0);
//            TreesMapUser u1 = new TreesMapUser("Jimmy", "qq", "ww", 1);
//            TreesMapUser u2 = new TreesMapUser("Louis", "aa", "ss", 0);
//            TreesMapUser u3 = new TreesMapUser("Simon", "ZZ", "XX", 0);
//            db2.addUser(u0);
//            db2.addUser(u1);
//            db2.addUser(u2);
//            db2.addUser(u3);
//            db2.close();

//            TreesMapUser user = new TreesMapUser();
//            try {
//                UserDBHandler db = new UserDBHandler(this);
//                user = db.findUserByUsername(username);
//                db.close();
//            } catch(Exception e) {
//                mUsernameView.setError(getString(R.string.error_user_not_found));
//                focusView = mUsernameView;
//                focusView.requestFocus();
//            }

            getUserByName(gUsername);



        }
    }

    private void finishLogin(TreesMapUser user){

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("aUserID", String.valueOf(user.getId()));
        Log.d(TAG, "" + user.getId());
        intent.putExtra("isAdmin", isAdmin);
        startActivity(intent);
        finish();
    }

    public void getUserByName(String username) {

        String url = USER_DB + URL_FIND_USER_BY_USERNAME + username;
        Log.d(TAG, "getUserByUsername URL: " + url);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());


                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject user = (JSONObject)response.getJSONObject("aUser");

                    int id = user.getInt("id");
                    String name = user.getString("name");
                    String username = user.getString("username");
                    String password = user.getString("password");
                    int admin = user.getInt("admin");

                    TreesMapUser aUser = new TreesMapUser(id, name, username, password, admin);

                    Log.d(TAG, "getuserbyid user id: " + aUser.getId());

                    globalUser = aUser;

                    Log.d(TAG, "*** getUserByUsername, Success! ***");

                    authenticateUser(aUser, gPassword);



                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "JSONException" + e.getMessage().toString());
                    mUsernameView.setError(getString(R.string.error_user_not_found));
                    View focusView = mUsernameView;
                    focusView.requestFocus();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
//                Log.d(TAG, "Response.ErrorListener" + error.getMessage().toString());

            }
        });

        queue.add(req);
    }

    public void authenticateUser(TreesMapUser user, String password) {
        if(globalUser == null) {
            Log.d(TAG, "Error: User Not Found");
        } else {

            if(password.equals(user.getPassword())) {
                this.isAdmin = false;
                if(user.getAdmin() == 1) {
                    this.isAdmin = true;
                    Log.d(TAG, "isAdmin: " + isAdmin);
                }
                finishLogin(user);
            } else {
                mPasswordView.setError(getString(R.string.error_password_Incorrect));
                View focusView = mPasswordView;
                focusView.requestFocus();
            }

        }
    }

}

