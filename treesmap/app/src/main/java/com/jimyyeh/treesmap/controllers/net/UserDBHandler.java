package com.jimyyeh.treesmap.controllers.net;

/**
 * Created by jimyyeh on 29/07/2016.
 */

//Handler for user sqlite database, not in use anymore

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jimyyeh.treesmap.models.TreesMapUser;
import com.jimyyeh.treesmap.models.TreesMapUser;

import java.util.ArrayList;
import java.util.List;

//public class UserDBHandler extends SQLiteOpenHelper{
//
//    // Database Version
//    private static final int DATABASE_VERSION = 1;
//    // Database Name
//    private static final String DATABASE_NAME = "User";
//    // Contacts table name
//    private static final String TABLE_NAME = "User";
//    // user Table Columns names
//    private static final String KEY_ID = "id";
//    private static final String KEY_NAME = "name";
//    private static final String KEY_USERNAME = "username";
//    private static final String KEY_PASSWORD = "password";
//    private static final String KEY_ADMIN = "admin";
//
//
//
//    public UserDBHandler(Context context) {
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//    }
//
//    @Override
//    public void onCreate(SQLiteDatabase db) {
//        String CREATE_userS_TABLE =
//                "CREATE TABLE " + TABLE_NAME
//                + "("
//                + KEY_ID + " INTEGER PRIMARY KEY,"
//                + KEY_NAME + " TEXT,"
//                + KEY_USERNAME + " TEXT,"
//                + KEY_PASSWORD + " TEXT,"
//                + KEY_ADMIN + " INTEGER"
//                + ")";
//        db.execSQL(CREATE_userS_TABLE);
//    }
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        // Drop older table if existed
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
//        // Creating tables again
//        onCreate(db);
//    }
//
//    // Adding new user
//    public void addUser(TreesMapUser user) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_NAME, user.getName());
//        values.put(KEY_USERNAME, user.getUsername());
//        values.put(KEY_PASSWORD, user.getPassword());
//        values.put(KEY_ADMIN, user.getAdmin());
//
//        // Inserting Row
//        db.insert(TABLE_NAME, null, values);
//        db.close();
//    }
//
//    // Getting one user
//    public TreesMapUser findUserById(int id) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_NAME, new String[] {
//                KEY_ID,
//                KEY_NAME,
//                KEY_USERNAME,
//                KEY_PASSWORD,
//                KEY_ADMIN,
//
//        }, KEY_ID + "=?", new String[] { String.valueOf(id) }, null, null, null, null);
//
//        if (cursor != null) {
//            cursor.moveToFirst();
//        }
//
//        TreesMapUser user = new TreesMapUser(
//                Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1),
//                cursor.getString(2),
//                cursor.getString(3),
//                Integer.parseInt(cursor.getString(4))
//                );
//        cursor.close();
//        db.close();
//        return user;
//    }
//
//    public TreesMapUser findUserByUsername(String username) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_NAME, new String[] {
//                KEY_ID,
//                KEY_NAME,
//                KEY_USERNAME,
//                KEY_PASSWORD,
//                KEY_ADMIN,
//
//        }, KEY_USERNAME + "=?", new String[] { String.valueOf(username) }, null, null, null, null);
//
//        if (cursor != null) {
//            cursor.moveToFirst();
//        }
//
//        TreesMapUser user = new TreesMapUser(
//                Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1),
//                cursor.getString(2),
//                cursor.getString(3),
//                Integer.parseInt(cursor.getString(4))
//        );
//        cursor.close();
//        db.close();
//        return user;
//    }
//
//    // Getting All users
//    public List<TreesMapUser> getAllUsers() {
//        List<TreesMapUser> userList = new ArrayList<TreesMapUser>();
//        // Select All Query
//        String selectQuery = "SELECT * FROM " + TABLE_NAME;
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                TreesMapUser user = new TreesMapUser();
//                user.setId(Integer.parseInt(cursor.getString(0)));
//                user.setName(cursor.getString(1));
//                user.setUsername(cursor.getString(2));
//                user.setPassword(cursor.getString(3));
//                user.setAdmin(Integer.parseInt(cursor.getString(4)));
//
//                // Adding user to list
//                userList.add(user);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        // return user list
//        return userList;
//    }
//
//    // Updating a user
//    public int updateuser(TreesMapUser user) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_NAME, user.getName());
//        values.put(KEY_USERNAME, user.getUsername());
//        values.put(KEY_PASSWORD, user.getPassword());
//        values.put(KEY_ADMIN, user.getAdmin());
//
//        // updating row
//        return db.update(TABLE_NAME, values, KEY_ID + " = ?", new String[]{String.valueOf(user.getId())});
//    }
//
//    // Deleting a user
//    public void deleteuser(TreesMapUser user) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_NAME, KEY_ID + " = ?", new String[] { String.valueOf(user.getId()) });
//        db.close();
//    }
//
//    public void deleteAllusers(){
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.execSQL("DELETE FROM "+ TABLE_NAME);
//        db.close();
//    }
//}

