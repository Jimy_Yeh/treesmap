package com.jimyyeh.treesmap.models.events;

/**
 * Created by jimyyeh on 16/09/2016.
 */

public class DeleteResultEvent implements IEvent {

    private int result;

    public DeleteResultEvent(int result) {
        this.result = result;

    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }


}
