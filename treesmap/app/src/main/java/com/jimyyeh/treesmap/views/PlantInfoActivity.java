package com.jimyyeh.treesmap.views;

import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.jimyyeh.treesmap.R;
import com.jimyyeh.treesmap.models.Plantation;
import com.jimyyeh.treesmap.models.TreesMapUser;
import com.jimyyeh.treesmap.models.events.DeletePlantEvent;
import com.jimyyeh.treesmap.models.events.DeleteResultEvent;
import com.jimyyeh.treesmap.models.events.SavePlantEvent;
import com.jimyyeh.treesmap.models.events.UpdateResultEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlantInfoActivity extends AppCompatActivity {

    private static final String TAG = "PlantInfoActivity";
    List<String> plantTypes = Arrays.asList("Tree", "Grass", "Flower");
    List<String> plantStatus = new ArrayList<>();

    List<String> plantWorkhour = new ArrayList<>();
    List<String> plantAssignto = new ArrayList<>();

    ArrayList<TreesMapUser> userList = new ArrayList<>();

    Plantation selectedPlant = new Plantation();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_info);

        (findViewById(R.id.plant_info_layout)).requestFocus();

        EventBus.getDefault().register(this);
        //populate array list with number of hours from 1 to 20
        for(int i = 1; i < 21; i++){
            plantWorkhour.add("" + i);
        }

        for(int i = 0; i < 21; i++) {
            plantStatus.add("" + (i * 5));
        }

        userList = getIntent().getParcelableArrayListExtra("userArray");
        for(int i = 0; i < userList.size(); i++) {
            plantAssignto.add(userList.get(i).getName());
        }


        Bundle extras = getIntent().getExtras();
        selectedPlant = extras.getParcelable("selectedPlant");

        String initialType = selectedPlant.getType();
        String initialStatus = "" + selectedPlant.getStatus();
        String initialWorkhours = "" + selectedPlant.getWorkTime();
        String initialAssignto = extras.getString("plantAssignto");
        String initialDescription = selectedPlant.getDescription();
        int initialHourspent = selectedPlant.getHourSpent();


        final Spinner spinnerType = (Spinner) findViewById(R.id.spinner_plant_type);
        ArrayAdapter<String> spinnerTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, plantTypes);
        spinnerTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(spinnerTypeAdapter);
        if (!initialType.equals(null)) {
            int spinnerPosition = spinnerTypeAdapter.getPosition(initialType);
            spinnerType.setSelection(spinnerPosition);
        }

        final Spinner spinnerStatus = (Spinner) findViewById(R.id.spinner_plant_status);
        ArrayAdapter<String> spinnerStatusAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, plantStatus);
        spinnerStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(spinnerStatusAdapter);
        if (!initialStatus.equals(null)) {
            int spinnerPosition = spinnerStatusAdapter.getPosition(initialStatus);
            spinnerStatus.setSelection(spinnerPosition);
        }

        final Spinner spinnerWorkhour = (Spinner) findViewById(R.id.spinner_plant_workhour);
        ArrayAdapter<String> spinnerWorkhourAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, plantWorkhour);
        spinnerWorkhourAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWorkhour.setAdapter(spinnerWorkhourAdapter);
        if (!initialWorkhours.equals(null)) {
            int spinnerPosition = spinnerWorkhourAdapter.getPosition(initialWorkhours);
            spinnerWorkhour.setSelection(spinnerPosition);
        }

        final Spinner spinnerAssignto = (Spinner) findViewById(R.id.spinner_plant_assignto);
        ArrayAdapter<String> spinnerAssigntoAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, plantAssignto);
        spinnerAssigntoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAssignto.setAdapter(spinnerAssigntoAdapter);
        if (!initialAssignto.equals(null)) {
            int spinnerPosition = spinnerAssigntoAdapter.getPosition(initialAssignto);
            spinnerAssignto.setSelection(spinnerPosition);
        }

        EditText descriptionEditText = (EditText) findViewById(R.id.description_area);
        if(!initialDescription.equals(null)) {
            descriptionEditText.setText(initialDescription);
        }

        EditText hourspentEditText = (EditText) findViewById(R.id.plant_hourspent_edittext);
        hourspentEditText.setText(String.format("%d", initialHourspent));


        findViewById(R.id.action_save_plant).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pAssignto = "";
                String pDescription = "";

                String selectedUserName = spinnerAssignto.getSelectedItem().toString();
                for(TreesMapUser user: userList){
                    if(user.getName().equals(selectedUserName)) {
                        pAssignto = "" + user.getId();
                        break;
                    }
                }

                EditText descriptionArea = (EditText) findViewById(R.id.description_area);
                pDescription = descriptionArea.getText().toString();

                EditText plantHourSpent = (EditText) findViewById(R.id.plant_hourspent_edittext);

                int pID = selectedPlant.getId();
                String pType = spinnerType.getSelectedItem().toString();
                int pStatus = Integer.parseInt(spinnerStatus.getSelectedItem().toString());
                int pWorkhour = Integer.parseInt(spinnerWorkhour.getSelectedItem().toString());
                int pHourSpent = Integer.parseInt(plantHourSpent.getText().toString());


                Plantation savePlant = new Plantation(pID, pType, selectedPlant.getLatitude(), selectedPlant.getLongitude(), pStatus, pWorkhour, pDescription, pAssignto, pHourSpent);
                EventBus.getDefault().post(new SavePlantEvent(savePlant));

            }
        });

        findViewById(R.id.action_delete_plant).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new DeletePlantEvent(selectedPlant.getId()));
            }
        });





    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    /**
     * Called When a plant update response
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(UpdateResultEvent event) {
        int result = event.getResult();

        if(result != 1) {
            AlertDialog alertDialog = new AlertDialog.Builder(PlantInfoActivity.this).create();
            alertDialog.setTitle(getString(R.string.save_fail_string));
//            alertDialog.setMessage(errorString);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.dialog_ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            alertDialog.show();


        }else {

            finishActivity();
        }

    }

    /**
     * Called When admin status is set
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(DeleteResultEvent event) {
        int result = event.getResult();

        if(result != 1) {
            AlertDialog alertDialog = new AlertDialog.Builder(PlantInfoActivity.this).create();
            alertDialog.setTitle(getString(R.string.delete_fail_string));

            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.dialog_ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            alertDialog.show();


        }else {
            AlertDialog alertDialog = new AlertDialog.Builder(PlantInfoActivity.this).create();
            alertDialog.setTitle(getString(R.string.delete_success_string));

            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.dialog_ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finishActivity();
                        }
                    });
            alertDialog.show();

        }

    }

    public void finishActivity() {
        this.finish();
    }
}
