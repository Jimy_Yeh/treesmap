package com.jimyyeh.treesmap.models.events;

/**
 * Created by jimyyeh on 24/08/2016.
 */

public class UpdateResultEvent implements IEvent {

    private int result;

    public UpdateResultEvent(int result) {
        this.result = result;

    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
