package com.jimyyeh.treesmap.models.map;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.jimyyeh.treesmap.helpers.MapHelper;

import java.util.ArrayList;
import java.util.List;



public class SearchParams {
    private static final String TAG = "SearchParams";

    public static final int DEFAULT_RADIUS = 100;
    public static final double DISTANCE = 173.1;

    private int mRadius;
    private LatLng mCenter;

    public SearchParams(int radius, LatLng center){
        mRadius = radius;
        mCenter = center;
    }

    public List<LatLng> getSearchArea(){
        List<LatLng> searchArea = new ArrayList<>();
        searchArea.add(mCenter);


        return searchArea;
    }
}

