package com.jimyyeh.treesmap.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class TreesMapUser implements Parcelable {

    private int id;
    private String name;
    private String username;
    private String password;
    private int admin;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public TreesMapUser(){

    }

    public TreesMapUser(int id, String name, String username, String password, int admin) {

        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.admin = admin;

    }
    public TreesMapUser(String name, String username, String password, int admin) {

        this.name = name;
        this.username = username;
        this.password = password;
        this.admin = admin;
    }

    private TreesMapUser(Parcel in){
        this.id = in.readInt();
        this.name = in.readString();
        this.username = in.readString();
        this.password = in.readString();
        this.admin = in.readInt();
    }

    public static final Parcelable.Creator<TreesMapUser> CREATOR =
            new Parcelable.Creator<TreesMapUser>() {

                @Override
                public TreesMapUser createFromParcel(Parcel source) {
                    return new TreesMapUser(source);
                }

                @Override
                public TreesMapUser[] newArray(int size) {
                    return new TreesMapUser[size];
                }

            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(this.id);
        out.writeString(this.name);
        out.writeString(this.username);
        out.writeString(this.password);
        out.writeInt(this.admin);
    }


}
