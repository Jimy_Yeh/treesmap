package com.jimyyeh.treesmap.models.events;


public class IsAdminEvent implements IEvent {

    private boolean isAdmin;



    public IsAdminEvent(boolean isAdmin) {
        this.isAdmin = isAdmin;

    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
