package com.jimyyeh.treesmap.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class Plantation implements Parcelable {

    private int id;
    private String type;
    private double latitude;
    private double longitude;
    private int status;
    private int workTime;
    private String description;
    private String assignedTo;
    private int hourSpent;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longatude) {
        this.longitude = longatude;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getWorkTime() {
        return workTime;
    }

    public void setWorkTime(int workTime) {
        this.workTime = workTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public int getHourSpent() {
        return hourSpent;
    }

    public void setHoursSpent(int hourSpent) {
        this.hourSpent = hourSpent;
    }

    public Plantation(){

    }

    public Plantation(int id, String type, double latitude, double longitude, int status, int workTime, String description, String assignedTo, int hourSpent) {

        this.id = id;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.workTime = workTime;
        this.description = description;
        this.assignedTo = assignedTo;
        this.hourSpent = hourSpent;

    }
    public Plantation(String type, double latitude, double longitude, int status, int workTime, String description, String assignedTo, int hourSpent) {


        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.workTime = workTime;
        this.description = description;
        this.assignedTo = assignedTo;
        this.hourSpent = hourSpent;

    }

    public Plantation(Plantation plant) {

        this.type = plant.getType();
        this.latitude = plant.getLatitude();
        this.longitude = plant.getLongitude();
        this.status = plant.getStatus();
        this.workTime = plant.getWorkTime();
        this.description = plant.getDescription();
        this.assignedTo = plant.getAssignedTo();
        this.hourSpent = plant.getHourSpent();

    }

    private Plantation(Parcel in) {
        this.id = in.readInt();
        this.type = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.status = in.readInt();
        this.workTime = in.readInt();
        this.description = in.readString();
        this.assignedTo = in.readString();
        this.hourSpent = in.readInt();

    }

    public static final Parcelable.Creator<Plantation> CREATOR =
            new Parcelable.Creator<Plantation>() {

                @Override
                public Plantation createFromParcel(Parcel source) {
                    return new Plantation(source);
                }

                @Override
                public Plantation[] newArray(int size) {
                    return new Plantation[size];
                }

            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(this.id);
        out.writeString(this.type);
        out.writeDouble(this.latitude);
        out.writeDouble(this.longitude);
        out.writeInt(this.status);
        out.writeInt(this.workTime);
        out.writeString(this.description);
        out.writeString(this.assignedTo);
        out.writeInt(this.hourSpent);
    }
}
