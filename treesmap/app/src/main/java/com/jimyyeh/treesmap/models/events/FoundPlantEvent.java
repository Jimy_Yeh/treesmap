package com.jimyyeh.treesmap.models.events;

import com.jimyyeh.treesmap.models.Plantation;

import java.util.List;


public class FoundPlantEvent implements IEvent {

    private List<Plantation> plantList;



    public FoundPlantEvent(List<Plantation> plantList) {
        this.plantList = plantList;

    }

    public List<Plantation> getPlantList() {
        return plantList;
    }

    public void setCatchablePokemon(List<Plantation> plantList) {
        this.plantList = plantList;
    }


}
