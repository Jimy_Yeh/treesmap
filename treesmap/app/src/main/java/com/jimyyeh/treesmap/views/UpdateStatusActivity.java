package com.jimyyeh.treesmap.views;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.jimyyeh.treesmap.R;
import com.jimyyeh.treesmap.models.Plantation;
import com.jimyyeh.treesmap.models.events.SavePlantEvent;
import com.jimyyeh.treesmap.models.events.UpdateResultEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class UpdateStatusActivity extends AppCompatActivity {

    private static final String TAG = "UpdateStatusActivity";

    Plantation selectedPlant = new Plantation();

    List<String> plantAdditionalWorkhour = new ArrayList<>();
    List<String> plantStatus = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_status);

        (findViewById(R.id.update_status_layout)).requestFocus();

        EventBus.getDefault().register(this);

        for(int i = 0; i < 13; i++){
            plantAdditionalWorkhour.add("" + i);
        }

        for(int i = 0; i < 21; i++) {
            plantStatus.add("" + (i * 5));
        }

        Bundle extras = getIntent().getExtras();
        selectedPlant = extras.getParcelable("selectedPlant");

        String initialType = selectedPlant.getType();
        String initialStatus = "" + selectedPlant.getStatus();
        String initialWorkhours = "" + selectedPlant.getWorkTime();
        String initialDescription = selectedPlant.getDescription();
        int initialHourspent = selectedPlant.getHourSpent();

        TextView typeTextView = (TextView) findViewById(R.id.textview_plant_type);
        typeTextView.setText(initialType);

        TextView workhourTextView = (TextView) findViewById(R.id.textview_plant_workhour);
        workhourTextView.setText(initialWorkhours);

        TextView hourspentTextView = (TextView) findViewById(R.id.textview_plant_hourspent);
        hourspentTextView.setText(String.format("%d", initialHourspent));

        final Spinner spinnerAdditionalHours = (Spinner) findViewById(R.id.spinner_addtional_hourspent);
        ArrayAdapter<String> spinnerAdditionalHoursAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, plantAdditionalWorkhour);
        spinnerAdditionalHoursAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdditionalHours.setAdapter(spinnerAdditionalHoursAdapter);


        final Spinner spinnerStatus = (Spinner) findViewById(R.id.spinner_plant_status);
        ArrayAdapter<String> spinnerStatusAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, plantStatus);
        spinnerStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(spinnerStatusAdapter);
        if(initialHourspent >= 0) {
            int spinnerPosition = spinnerStatusAdapter.getPosition("" + initialStatus);
            spinnerStatus.setSelection(spinnerPosition);
        }

        EditText descriptionEditText = (EditText) findViewById(R.id.description_area);
        if(!initialDescription.equals(null)) {
            descriptionEditText.setText(initialDescription);
        }

        findViewById(R.id.action_save_plant).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                EditText descriptionArea = (EditText) findViewById(R.id.description_area);
                String pDescription = descriptionArea.getText().toString();


                int pStatus = Integer.parseInt(spinnerStatus.getSelectedItem().toString());
                int pAdditionalHourspent = Integer.parseInt(spinnerAdditionalHours.getSelectedItem().toString());



                Plantation savePlant = new Plantation(selectedPlant.getId(), selectedPlant.getType(), selectedPlant.getLatitude(), selectedPlant.getLongitude(), pStatus, selectedPlant.getWorkTime(), pDescription, selectedPlant.getAssignedTo(), selectedPlant.getHourSpent() + pAdditionalHourspent);
                EventBus.getDefault().post(new SavePlantEvent(savePlant));

            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    /**
     * Called When admin status is set
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(UpdateResultEvent event) {
        int result = event.getResult();

        if (result != 1) {
            AlertDialog alertDialog = new AlertDialog.Builder(UpdateStatusActivity.this).create();
            alertDialog.setTitle(getString(R.string.save_fail_string));
//            alertDialog.setMessage(errorString);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.dialog_ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            alertDialog.show();


        } else {

            this.finish();
        }
    }
}
