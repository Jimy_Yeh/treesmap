package com.jimyyeh.treesmap.models.events;

import com.google.android.gms.maps.model.LatLng;

public class SearchInPosition {

    private LatLng position;

    public SearchInPosition(LatLng position) {
        this.position = position;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }
}
