package com.jimyyeh.treesmap.controllers.net;

/**
 * Created by jimyyeh on 29/07/2016.
 */

//Handler for plant sqlite database, not in use anymore

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jimyyeh.treesmap.models.Plantation;

import java.util.ArrayList;
import java.util.List;

//public class PlantDBHandler extends SQLiteOpenHelper{
//
//    // Database Version
//    private static final int DATABASE_VERSION = 1;
//    // Database Name
//    private static final String DATABASE_NAME = "Plantation";
//    // Contacts table name
//    private static final String TABLE_NAME = "Plants";
//    // Plant Table Columns names
//    private static final String KEY_ID = "id";
//    private static final String KEY_TYPE = "type";
//    private static final String KEY_LAT = "latitude";
//    private static final String KEY_LONG = "longitude";
//    private static final String KEY_STATUS = "status";
//    private static final String KEY_WORKTIME = "worktime";
//    private static final String KEY_DESCRIPTION = "description";
//    private static final String KEY_ASSIGNEDTO = "assignedto";
//    private static final String KEY_HOURSPENT = "hourspent";
//
//    public PlantDBHandler(Context context) {
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//    }
//
//    @Override
//    public void onCreate(SQLiteDatabase db) {
//        String CREATE_PLANTS_TABLE =
//                "CREATE TABLE " + TABLE_NAME
//                + "("
//                + KEY_ID + " INTEGER PRIMARY KEY,"
//                + KEY_TYPE + " TEXT,"
//                + KEY_LAT + " DOUBLE,"
//                + KEY_LONG + " DOUBLE,"
//                + KEY_STATUS + " INT,"
//                + KEY_WORKTIME + " INT,"
//                + KEY_DESCRIPTION + " TEXT,"
//                + KEY_ASSIGNEDTO + " TEXT,"
//                + KEY_HOURSPENT + " INT"
//                + ")";
//        db.execSQL(CREATE_PLANTS_TABLE);
//    }
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        // Drop older table if existed
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
//        // Creating tables again
//        onCreate(db);
//    }
//
//    // Adding new plant
//    public void addPlant(Plantation plant) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_TYPE, plant.getType());
//        values.put(KEY_LAT, plant.getLatitude());
//        values.put(KEY_LONG, plant.getLongitude());
//        values.put(KEY_STATUS, plant.getStatus());
//        values.put(KEY_WORKTIME, plant.getWorkTime());
//        values.put(KEY_DESCRIPTION, plant.getDescription());
//        values.put(KEY_ASSIGNEDTO, plant.getAssignedTo());
//        values.put(KEY_HOURSPENT, plant.getHourSpent());
//
//        // Inserting Row
//        db.insert(TABLE_NAME, null, values);
//        db.close(); // Closing database connection
//    }
//
//    // Getting one plant
//    public Plantation getPlantById(int id) {
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.query(TABLE_NAME, new String[] { KEY_ID, KEY_TYPE, KEY_LAT, KEY_LONG, KEY_STATUS, KEY_WORKTIME, KEY_DESCRIPTION, KEY_ASSIGNEDTO, KEY_HOURSPENT }, KEY_ID + "=?", new String[] { String.valueOf(id) }, null, null, null, null);
//
//        if (cursor != null) {
//            cursor.moveToFirst();cursor.moveToFirst();
//        }
//
//        Plantation plant = new Plantation(
//                Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1),
//                Double.parseDouble(cursor.getString(2)),
//                Double.parseDouble(cursor.getString(3)),
//                cursor.getInt(4),
//                cursor.getInt(5),
//                cursor.getString(6),
//                cursor.getString(7),
//                cursor.getInt(8)
//                );
//        cursor.close();
//        return plant;
//    }
//
//    // Getting All Plants
//    public List<Plantation> getAllPlants() {
//        List<Plantation> plantList = new ArrayList<Plantation>();
//        // Select All Query
//        String selectQuery = "SELECT * FROM " + TABLE_NAME;
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Plantation plant = new Plantation();
//                plant.setId(Integer.parseInt(cursor.getString(0)));
//                plant.setType(cursor.getString(1));
//                plant.setLatitude(Double.parseDouble(cursor.getString(2)));
//                plant.setLongitude(Double.parseDouble(cursor.getString(3)));
//                plant.setStatus(cursor.getInt(4));
//                plant.setWorkTime(Integer.parseInt(cursor.getString(5)));
//                plant.setDescription(cursor.getString(6));
//                plant.setAssignedTo(cursor.getString(7));
//                plant.setHoursSpent(cursor.getInt(8));
//
//                // Adding plant to list
//                plantList.add(plant);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        db.close();
//        // return plant list
//        return plantList;
//    }
//
//    public List<Plantation> getPlantsByUserId(String userId) {
//
//        List<Plantation> plantList = new ArrayList<Plantation>();
//        // Select All Query
//        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_ASSIGNEDTO + " = " + userId;
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Plantation plant = new Plantation();
//                plant.setId(Integer.parseInt(cursor.getString(0)));
//                plant.setType(cursor.getString(1));
//                plant.setLatitude(Double.parseDouble(cursor.getString(2)));
//                plant.setLongitude(Double.parseDouble(cursor.getString(3)));
//                plant.setStatus(Integer.parseInt(cursor.getString(4)));
//                plant.setWorkTime(Integer.parseInt(cursor.getString(5)));
//                plant.setDescription(cursor.getString(6));
//                plant.setAssignedTo(cursor.getString(7));
//                plant.setHoursSpent(cursor.getInt(8));
//
//                // Adding plant to list
//                plantList.add(plant);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        db.close();
//        // return plant list
//        return plantList;
//    }
//
//    // Updating a plant
//    public int updatePlant(Plantation plant) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_TYPE, plant.getType());
//        values.put(KEY_LAT, plant.getLatitude());
//        values.put(KEY_LONG, plant.getLongitude());
//        values.put(KEY_STATUS, plant.getStatus());
//        values.put(KEY_WORKTIME, plant.getWorkTime());
//        values.put(KEY_DESCRIPTION, plant.getDescription());
//        values.put(KEY_ASSIGNEDTO, plant.getAssignedTo());
//        values.put(KEY_HOURSPENT, plant.getHourSpent());
//
//        // updating row
//        return db.update(TABLE_NAME, values, KEY_ID + " = ?", new String[]{String.valueOf(plant.getId())});
//    }
//
//    // Deleting a plant
//    public void deletePlant(Plantation plant) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_NAME, KEY_ID + " = ?", new String[] { String.valueOf(plant.getId()) });
//        db.close();
//    }
//
//    public void deleteAllPlants(){
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.execSQL("DELETE FROM "+ TABLE_NAME);
//    }
//}
//
