package com.jimyyeh.treesmap.views;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.jimyyeh.treesmap.R;
import com.jimyyeh.treesmap.models.Plantation;
import com.jimyyeh.treesmap.models.TreesMapUser;
import com.jimyyeh.treesmap.models.events.DeletePlantEvent;
import com.jimyyeh.treesmap.models.events.DeleteResultEvent;
import com.jimyyeh.treesmap.models.events.FoundPlantEvent;
import com.jimyyeh.treesmap.models.events.GetUserListEvent;
import com.jimyyeh.treesmap.models.events.InfoWindowClickedEvent;
import com.jimyyeh.treesmap.models.events.IsAdminEvent;
import com.jimyyeh.treesmap.models.events.SavePlantEvent;
import com.jimyyeh.treesmap.models.events.SearchInPosition;
import com.jimyyeh.treesmap.models.events.UpdateResultEvent;
import com.jimyyeh.treesmap.views.map.MapWrapperFragment;
import com.jimyyeh.treesmap.views.settings.SettingsActivity;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class MainActivity extends BaseActivity {
    private static final String TAG = "MainActivity";
    private static final String MAP_FRAGMENT_TAG = "MapFragment";
    private Snackbar treeSnackbar;
    public boolean isAdmin = false;
    Bundle extras = new Bundle();

    private static final String PLANT_DB = "http://10.0.2.2:1337/plantation";
    private static final String URL_ALL_PLANTS = "/getall";
    private static final String URL_FIND_PLANT_BY_USER_ID = "/findplantbyuserid?userId=";
    private static final String URL_FIND_PLANT_BY_ID = "/findplantbyid?id=";
    private static final String URL_UPDATE_PLANT = "/updateplant?";
    private static final String URL_DELETE_PLANT = "/deleteplant?id=";

    private static final String USER_DB = "http://10.0.2.2:1337/treesmapuser";
    private static final String URL_ALL_USERS = "/findalluser";
    private static final String URL_FIND_USER_BY_ID = "/finduserbyid?id=";


    private List<TreesMapUser> userList= new LinkedList<>();
    private ArrayList<TreesMapUser> userArrayList = new ArrayList<>();
    private List<Plantation> plantsList = new LinkedList<>();

    public RequestQueue queue;

    public Plantation globalPlant;
    public TreesMapUser globalUser;

    public LatLng startupLocation = new LatLng(-27.467365, 153.024270);


    public void snackMe(String message,int duration){
        if (null == treeSnackbar || treeSnackbar.getDuration() != duration){
            View rootView = findViewById(R.id.main_container);
            treeSnackbar = Snackbar.make(rootView,"",duration);
        }
        treeSnackbar.setText(message);
        treeSnackbar.show();
    }

    public void snackMe(String message){
        snackMe(message, Snackbar.LENGTH_LONG);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        FragmentManager fragmentManager = getSupportFragmentManager();
        MapWrapperFragment mapWrapperFragment = (MapWrapperFragment) fragmentManager.findFragmentByTag(MAP_FRAGMENT_TAG);
        if(mapWrapperFragment == null) {
            mapWrapperFragment = MapWrapperFragment.newInstance();
        }
        fragmentManager.beginTransaction().replace(R.id.main_container,mapWrapperFragment, MAP_FRAGMENT_TAG)
                .commit();

        EventBus.getDefault().register(this);

        queue = Volley.newRequestQueue(this, new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(URL url) throws IOException {
                HttpURLConnection connection = super.createConnection(url);
                connection.setRequestProperty("Accept-Encoding", "application/json; charset=UTF-8");

                return connection;
            }
        });

        //get user list
        getUsers(USER_DB + URL_ALL_USERS);
    }

    @Override
    public void onResume(){
        super.onResume();
        EventBus.getDefault().post(new SearchInPosition(startupLocation));


    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    //region Menu Methods
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        } else if (id == R.id.action_relogin) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case 703:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "permission granted");
                }
                break;
        }
    }

//    public void addPlant(Plantation aPlant) {
//
//        String addPlantURL = PLANT_DB + URL_ADD_PLANT + "type=" + aPlant.getType() + "&latitude=" + aPlant.getLatitude() + "&longitude=" + aPlant.getLongitude() + "&status=" + aPlant.getStatus() + "&workTime=" + aPlant.getWorkTime() + "&description=" + aPlant.getDescription() + "&assignedTo=" + aPlant.getAssignedTo() + "&hourSpent=" + aPlant.getHourSpent();
//        Log.d(TAG, "addPlantURL: " + addPlantURL);
//
//
//        JsonObjectRequest addPlantRequest = new JsonObjectRequest(Request.Method.GET, addPlantURL, null, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject response) {
//                Log.d(TAG, response.toString());
//
//                try {
//                    // Parsing json object response
//                    // response will be a json object
//                    JSONObject plant = response.getJSONObject("aPlant");
//                    Log.d(TAG, "add Plant, id: " + plant.getInt("id"));
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Log.d(TAG, "JSONException" + e.getMessage().toString());
//
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
//                Log.d(TAG, "Response.ErrorListener" + error.getMessage().toString());
//
//            }
//        });
//
//        queue.add(addPlantRequest);
//    }

    public void updatePlant(Plantation aPlant) {

        String updatePlantURL = PLANT_DB + URL_UPDATE_PLANT + "id=" + (int)aPlant.getId() + "&type=" + aPlant.getType().toString() + "&latitude=" + (float)aPlant.getLatitude() + "&longitude=" + (float)aPlant.getLongitude() + "&status=" + (int)aPlant.getStatus() + "&workTime=" + (int)aPlant.getWorkTime() + "&description=" + aPlant.getDescription().toString() + "&assignedTo=" + aPlant.getAssignedTo().toString() + "&hourSpent=" + (int)aPlant.getHourSpent();
        Log.d(TAG, "updatePlantURL: " + updatePlantURL);
        Log.d(TAG, "updating plant's id: " + aPlant.getId());

        JsonObjectRequest updatePlantRequest = new JsonObjectRequest(Request.Method.GET, updatePlantURL, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                Log.d(TAG, "*** update success ***");
                snackMe("Saved.");

                EventBus.getDefault().post(new UpdateResultEvent(1));

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        });

        queue.add(updatePlantRequest);
    }


    public void getPlants(String url) {
        plantsList.clear();

        Log.d(TAG, "URL: "+ url);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray plantArray = response.getJSONArray("plantList");


                    for(int i = 0; i < plantArray.length(); i++) {
                        JSONObject plant = (JSONObject) plantArray.get(i);

                        int id = plant.getInt("id");
                        String type = plant.getString("type");
                        double latitude = plant.getDouble("latitude");
                        double longitude = plant.getDouble("longitude");
                        int status = plant.getInt("status");
                        int workTime = plant.getInt("workTime");
                        String description = plant.getString("description");
                        String assignedTo = plant.getString("assignedTo");
                        int hourSpent = plant.getInt("hourSpent");

                        Plantation aPlant = new Plantation(id, type, latitude, longitude, status, workTime, description, assignedTo, hourSpent);

                        plantsList.add(aPlant);
//                        Log.d(TAG, "" + aPlant.getType());
                        EventBus.getDefault().post(new FoundPlantEvent(plantsList));
                    }

                } catch (JSONException e) {
//                    e.printStackTrace();
                    Log.d(TAG, "JSONException: " + e.getMessage().toString());
                    snackMe("No plantation found.");

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.d(TAG, "Response.ErrorListener" + error.getMessage().toString());

            }
        });


        queue.add(req);

    }

    public void getPlantById(int id) {

        String url = PLANT_DB + URL_FIND_PLANT_BY_ID + id;
        Log.d(TAG, "getPlantById URL: " + url);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());


                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject plant = (JSONObject)response.getJSONObject("aPlant");

                    int id = plant.getInt("id");
                    String type = plant.getString("type");
                    double latitude = plant.getDouble("latitude");
                    double longitude = plant.getDouble("longitude");
                    int status = plant.getInt("status");
                    int workTime = plant.getInt("workTime");
                    String description = plant.getString("description");
                    String assignedTo = plant.getString("assignedTo");
                    int hourSpent = plant.getInt("hourSpent");

                    Plantation aPlant = new Plantation(id, type, latitude, longitude, status, workTime, description, assignedTo, hourSpent);

                    Log.d(TAG, "*** getplantbyid, Success! ***");
                    Log.d(TAG, "getplantbyid plant id: " + aPlant.getId());

                    globalPlant = aPlant;

                    getUserById(Integer.parseInt(globalPlant.getAssignedTo()));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "JSONException" + e.getMessage().toString());

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.d(TAG, "Response.ErrorListener: " + error.getMessage().toString());

            }
        });

        queue.add(req);
    }

    public void deletePlant(int id) {

        String deletePlantURL = PLANT_DB + URL_DELETE_PLANT + id;
        Log.d(TAG, "deletePlantURL: " + deletePlantURL);


        JsonObjectRequest deletePlantRequest = new JsonObjectRequest(Request.Method.GET, deletePlantURL, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                Log.d(TAG, "*** delete success ***");
                snackMe("Plant Deleted.");

                EventBus.getDefault().post(new DeleteResultEvent(1));

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());


            }
        });

        queue.add(deletePlantRequest);
    }



    public void getUsers(String url) {
        userList.clear();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray userArray = response.getJSONArray("allUsers");

                    for(int i = 0; i < userArray.length(); i++) {
                        JSONObject user = (JSONObject) userArray.get(i);

                        int id = user.getInt("id");
                        String name = user.getString("name");
                        String username = user.getString("username");
                        String password = user.getString("password");
                        int admin = user.getInt("admin");

                        TreesMapUser aUser = new TreesMapUser(id, name, username, password, admin);

                        userList.add(aUser);

                    }
                    EventBus.getDefault().post(new GetUserListEvent(userList));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "JSONException" + e.getMessage().toString());

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.d(TAG, "Response.ErrorListener" + error.getMessage().toString());

            }
        });

        queue.add(req);

    }

    public void getUserById(int id) {

        String url = USER_DB + URL_FIND_USER_BY_ID + id;
        Log.d(TAG, "getUserById URL: " + url);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());


                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject user = (JSONObject)response.getJSONObject("aUser");

                    int id = user.getInt("id");
                    String name = user.getString("name");
                    String username = user.getString("username");
                    String password = user.getString("password");
                    int admin = user.getInt("admin");

                    TreesMapUser aUser = new TreesMapUser(id, name, username, password, admin);

                    Log.d(TAG, "getuserbyid user id: " + aUser.getId());

                    globalUser = aUser;

                    Log.d(TAG, "*** getUserById, Success! ***");

                    startInfoWindow();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "JSONException" + e.getMessage().toString());

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.d(TAG, "Response.ErrorListener" + error.getMessage().toString());

            }
        });

        queue.add(req);
    }


    public void refreshUserArrayList() {
        userArrayList.clear();
        for(int i = 0; i < userList.size(); i++){
            userArrayList.add(userList.get(i));
        }

        Log.d(TAG, "refreshUserArrayList, Success!");
    }

    public void startInfoWindow() {

        if(globalUser != null && globalPlant != null && userArrayList.size() > 0) {

            if(isAdmin){
                Intent intent = new Intent(this, PlantInfoActivity.class);

                intent.putExtra("selectedPlant", globalPlant);
                intent.putExtra("plantAssignto", globalUser.getName());

                intent.putParcelableArrayListExtra("userArray", userArrayList);
                Log.d(TAG, "startInfoWindow, isAdmin, Success!");
                startActivity(intent);

            }else {
                Intent intent = new Intent(this, UpdateStatusActivity.class);

                intent.putExtra("selectedPlant", globalPlant);
                Log.d(TAG, "startInfoWindow, not Admin, Success!");
                startActivity(intent);
            }
        }else {
            Log.d(TAG, "startInfoWindow Error, variable = null");
        }
    }


    /**
     * Called When admin status is set
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(IsAdminEvent event) {
        isAdmin = event.isAdmin();

    }

    /**
     * Called When admin status is set
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(DeletePlantEvent event) {

        deletePlant(event.getId());

    }

    /**
     * Called When marker information window is clicked
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(InfoWindowClickedEvent event) {
        int id = event.getPlantID();

        refreshUserArrayList();

        getPlantById(id);

    }

    /**
     * Called When save button is pressed in
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(SavePlantEvent event) {

        Plantation plant = event.getPlant();

        int updateResult = 0;


        updatePlant(plant);


    }



    /**
     * Called to refresh
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(SearchInPosition event) {

        snackMe(getString(R.string.toast_searching));

        extras = getIntent().getExtras();
        isAdmin = extras.getBoolean("isAdmin");

        Log.d(TAG, "@SIP, admin: "+ isAdmin);
        EventBus.getDefault().post(new IsAdminEvent(isAdmin));



//        addPlant(getRandomPlant(-27.4710307000968, 153.024056644249));
//        addPlant(getRandomPlant(-27.4710307000968, 153.024056644249));


        if(isAdmin){
            getPlants(PLANT_DB + URL_ALL_PLANTS);

        } else {
            getPlants(PLANT_DB + URL_FIND_PLANT_BY_USER_ID + extras.getString("aUserID"));

        }
        Log.d(TAG, "plant count: " + plantsList.size());

    }
    //to generate random plants with location input
//    public Plantation getRandomPlant (double latitude, double longitude){
//
//        Random random = new Random();
//        String[] typeArray = {"Tree", "Flower", "Grass"};
//        int[] completenessArray = { 0, 50, 100};
//        String[] nameArray = {"Jimmy", "Louis", "Simon", "No one"};
//
//        String randomType = typeArray[random.nextInt(3)];
//        Log.d(TAG, String.format("type: %s", randomType));
//
//        int randomStatus = completenessArray[random.nextInt(3)];
//        Log.d(TAG, String.format("Completeness: %s", randomStatus));
//
//        String randomName = nameArray[random.nextInt(4)];
//        Log.d(TAG, String.format("Name: %s", randomName));
//
//        int randomWorkTime = random.nextInt(10) + 1;
//
//        double randomRange = (1 + (random.nextInt(100)-50)) * 0.0001;
//        double randomRange2 = (1 + (random.nextInt(100)-50)) * 0.0001;
//        Log.d(TAG, String.format("random 1: %.6f", randomRange));
//        Log.d(TAG, String.format("random 2: %.6f", randomRange2));
//
//        return new Plantation(randomType, latitude + randomRange, longitude + randomRange2, randomStatus, randomWorkTime, randomName + " considerings", "3", 0);
//
//    }



}

