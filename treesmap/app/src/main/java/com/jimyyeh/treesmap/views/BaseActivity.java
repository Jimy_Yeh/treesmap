package com.jimyyeh.treesmap.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jimyyeh.treesmap.controllers.map.LocationManager;
import com.jimyyeh.treesmap.controllers.net.PlantationManager;



public class BaseActivity extends AppCompatActivity {
    public static final String TAG = "BaseActivity";
    protected LocationManager.Listener locationListener;
    LocationManager locationManager;
    protected PlantationManager plantationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationManager = LocationManager.getInstance(this);


    }

    @Override
    public void onResume(){
        super.onResume();
        locationManager.onResume();
        if(locationListener != null){
            locationManager.register(locationListener);
        }
    }

    @Override
    public void onPause(){
        LocationManager.getInstance(this).onPause();
        if(locationListener != null){
            locationManager.unregister(locationListener);
        }
        super.onPause();
    }
}
