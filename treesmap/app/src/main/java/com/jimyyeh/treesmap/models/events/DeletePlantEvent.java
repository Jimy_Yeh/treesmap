package com.jimyyeh.treesmap.models.events;

/**
 * Created by jimyyeh on 16/09/2016.
 */

public class DeletePlantEvent implements IEvent {

    private int id;

    public DeletePlantEvent(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
