package com.jimyyeh.treesmap.models.events;


import com.jimyyeh.treesmap.models.TreesMapUser;

import java.util.List;

public class GetUserListEvent implements IEvent {

    private List<TreesMapUser> userList;



    public GetUserListEvent(List<TreesMapUser> userList) {
        this.userList = userList;

    }

    public List<TreesMapUser> getUserList() {
        return userList;
    }

    public void setUserList(List<TreesMapUser> userList) {
        this.userList = userList;
    }
}
