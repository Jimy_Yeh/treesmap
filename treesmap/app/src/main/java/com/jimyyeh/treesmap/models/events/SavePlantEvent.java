package com.jimyyeh.treesmap.models.events;

import com.jimyyeh.treesmap.models.Plantation;

/**
 * Created by jimyyeh on 24/08/2016.
 */

public class SavePlantEvent implements IEvent {

    private Plantation plant;

    public SavePlantEvent(Plantation plant) {
        this.plant = plant;

    }

    public Plantation getPlant() {
        return plant;
    }

    public void setPlant(Plantation plant) {
        this.plant = plant;
    }
}
