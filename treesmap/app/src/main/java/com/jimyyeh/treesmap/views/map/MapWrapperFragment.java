package com.jimyyeh.treesmap.views.map;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jimyyeh.treesmap.R;
import com.jimyyeh.treesmap.controllers.map.LocationManager;
import com.jimyyeh.treesmap.models.Plantation;
import com.jimyyeh.treesmap.models.TreesMapUser;
import com.jimyyeh.treesmap.models.events.FoundPlantEvent;
import com.jimyyeh.treesmap.models.events.GetUserListEvent;
import com.jimyyeh.treesmap.models.events.InfoWindowClickedEvent;
import com.jimyyeh.treesmap.models.events.IsAdminEvent;
import com.jimyyeh.treesmap.models.events.SearchInPosition;
import com.jimyyeh.treesmap.views.MainActivity;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 *
 * Use the {@link MapWrapperFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapWrapperFragment extends Fragment implements OnMapReadyCallback,
                                                            GoogleMap.OnMapLongClickListener, GoogleMap.OnInfoWindowClickListener,
                                                            ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int LOCATION_PERMISSION_REQUEST = 19;
    private static final String TAG = "MapWrapperFragment";
    private LocationManager locationManager;

    private View mView;
    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mGoogleMap;
    private Location mLocation = null;
    private Marker userSelectedPositionMarker = null;
    private ArrayList<Circle> userSelectedPositionCircles = new ArrayList<>();
    private boolean isAdmin = false;

    private List<TreesMapUser> userList = new LinkedList<>();
//    private ArrayList<TreesMapUser> userArrayList = new ArrayList<>();

    public static Snackbar plantSnackbar;
    public static int plantsFound = 0;
    public String markerStyle = "";

    public LatLng startupLocation = new LatLng(-27.467365, 153.024270);

    private void snackMe(String message, int duration){
        ((MainActivity)getActivity()).snackMe(message, duration);
    }
    private void snackMe(String message){
        snackMe(message, Snackbar.LENGTH_LONG);
    }


    public MapWrapperFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapWrapperFragment.
     */
    public static MapWrapperFragment newInstance() {
        MapWrapperFragment fragment = new MapWrapperFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        locationManager = LocationManager.getInstance(getContext());
        locationManager.register(new LocationManager.Listener() {
            @Override
            public void onLocationChanged(Location location) {
                if (mLocation == null) {
                    mLocation = location;

                    initMap();

                }
                else{
                    mLocation = location;
                }
            }

            @Override
            public void onLocationFetchFailed(@Nullable ConnectionResult connectionResult) {
                mLocation.setLongitude(startupLocation.longitude);
                mLocation.setLatitude(startupLocation.latitude);

                initMap();
                showLocationFetchFailed();
            }
        });
        // Inflate the layout for this fragment if the view is not null
        if (mView == null) mView = inflater.inflate(R.layout.fragment_map_wrapper, container, false);
        else {

        }

        // build the map
        if (mSupportMapFragment == null) {
            mSupportMapFragment = SupportMapFragment.newInstance();
            getChildFragmentManager().beginTransaction().replace(R.id.map, mSupportMapFragment).commit();
            mSupportMapFragment.setRetainInstance(true);
        }

        if (mGoogleMap == null) {
            mSupportMapFragment.getMapAsync(this);
        }

        FloatingActionButton locationFab = (FloatingActionButton) mView.findViewById(R.id.location_fab);
        locationFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLocation != null && mGoogleMap != null) {
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 15));
                }
                else{
                    Log.d(TAG, "animateCamera, onLocationFetchFailed" );
                    showLocationFetchFailed();
                }
            }
        });

        mView.findViewById(R.id.closeSuggestions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mView.findViewById(R.id.layoutSuggestions).setVisibility(View.GONE);
            }
        });

        return mView;
    }
    private void initMap(){

        plantSnackbar = Snackbar.make(getView(), "", Snackbar.LENGTH_LONG);


//        if (mLocation != null && mGoogleMap != null){
        if (mGoogleMap != null){
            if (ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                new AlertDialog.Builder(getActivity())
                        .setTitle("Enable Location Permission")
                        .setMessage("Please enable location permission to use this application")
                        .setPositiveButton("OK", null)
                        .show();
                return;
            }
            mGoogleMap.setMyLocationEnabled(true);

            LatLng loc;
            if(mLocation != null) {
                loc = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            }else {
                loc = new LatLng(startupLocation.latitude, startupLocation.longitude);
            }
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    //changing mLocation.getLatitude(), mLocation.getLongitude() to startupLocation

                    new LatLng(loc.latitude, loc.longitude), 15)
            );

            EventBus.getDefault().post(new SearchInPosition(startupLocation));
        } else {
            Log.d(TAG, "initMap, onLocationFetchFailed" );
            showLocationFetchFailed();
        }
    }

    private void setTreeMarkers(final List<Plantation> plantList){
        if (mGoogleMap != null) {

            plantsFound = 0;

            for (final Plantation plant : plantList) {

                //choose icon based on status
                int plantStatus = plant.getStatus();
                if(plantStatus == 0) {
                    markerStyle = "android.resource://com.jimyyeh.treesmap/drawable/map_marker_incomplete";
                }else if(plantStatus == 100) {
                    markerStyle = "android.resource://com.jimyyeh.treesmap/drawable/map_marker_complete";
                }else {
                    markerStyle = "android.resource://com.jimyyeh.treesmap/drawable/map_marker_in_progress";
                }


                //Showing images using glide
                Glide.with(getActivity())
                        .load(Uri.parse(markerStyle))
                        .asBitmap()
//                        .skipMemoryCache(false)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(new SimpleTarget<Bitmap>(60, 60) { // Width and height
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {

                                //show different information based on user privilege
                                String markerTitle = "title";
                                String markerSnippet = "snippet";


                                //if user is not admin
                                if(!isAdmin){
                                    markerTitle = String.format("%d", plant.getId());
                                    markerSnippet = String.format("%s, %d hours", plant.getType(), plant.getWorkTime());

                                } else { //if user is admin
                                    markerTitle = String.format("%d", plant.getId());
                                    for(TreesMapUser user: userList){
                                        if(Integer.parseInt(plant.getAssignedTo()) == user.getId()){
                                            markerSnippet = String.format("%s, Assigned to: %s", plant.getType(), user.getName());
                                        }
                                    }

                                }

                                Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(plant.getLatitude(), plant.getLongitude()))
                                        .title(markerTitle)
                                        .snippet(markerSnippet)
                                        .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                                        .anchor(0.5f, 0.5f));


                            }
                        });
                //Increase founded plant counter
                plantsFound++;

            }
            if(getView() != null) {
                String text = plantsFound + " plants have been found.";
                snackMe(text);
            }

//            updatePlantMarkers();
        } else {
            showMapNotInitializedError();
        }
    }



    private void showMapNotInitializedError() {
        if(getView() != null){
            snackMe(getString(R.string.toast_map_not_initialized), Snackbar.LENGTH_SHORT);
        }
    }

    private void showLocationFetchFailed() {
        if(getView() != null){
            snackMe(getString(R.string.toast_no_location), Snackbar.LENGTH_SHORT);
        }
    }

    /**
     * Called When admin status is set
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(IsAdminEvent event) {
        isAdmin = event.isAdmin();
        Log.d(TAG, "@event1, isAdmin: " + isAdmin);
    }

    /**
     * Called when userList is updated
     *
     * @param event The event information
     */
    @Subscribe
    public void onEvent(GetUserListEvent event) {
        userList.clear();
        userList.addAll(event.getUserList());
//        for(int i = 0; i < userList.size(); i++){
//            userArrayList.add(userList.get(i));
//        }
    }

    /**
     * Called whenever a FoundPlantEvent is posted to the bus. Posted when new plant that are found.
     *
     * @param event The event information
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FoundPlantEvent event) {
        setTreeMarkers(event.getPlantList());

    }



    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        UiSettings settings = mGoogleMap.getUiSettings();
        settings.setCompassEnabled(true);
        settings.setTiltGesturesEnabled(true);
        settings.setMyLocationButtonEnabled(false);
        //Handle long click
        mGoogleMap.setOnMapLongClickListener(this);
        mGoogleMap.setOnInfoWindowClickListener(this);
        //Disable for now coz is under FAB
        settings.setMapToolbarEnabled(false);
        initMap();
    }

    @Override
    public void onMapLongClick(LatLng position) {

        mGoogleMap.clear();

        //Sending event to MainActivity

        EventBus.getDefault().post(new SearchInPosition(position));
//        snackMe(getString(R.string.map_loading_plant));
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        EventBus.getDefault().post(new InfoWindowClickedEvent(Integer.parseInt(marker.getTitle())));
    }

    private void drawMarker(LatLng position){
        if (mGoogleMap != null) {

            userSelectedPositionMarker = mGoogleMap.addMarker(new MarkerOptions()
                    .position(position)
                    .title(getString(R.string.position_picked))
                    .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getContext().getResources(),
                            R.drawable.ic_my_location_white_24dp)))
                    .anchor(0.5f, 0.5f));
        } else {
            showMapNotInitializedError();
        }
    }

}

