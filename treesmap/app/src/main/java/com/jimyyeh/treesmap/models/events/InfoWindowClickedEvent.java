package com.jimyyeh.treesmap.models.events;

/**
 * Created by jimyyeh on 23/08/2016.
 */

public class InfoWindowClickedEvent implements IEvent {

    private int plantID;

    public InfoWindowClickedEvent(int plantID) {
        this.plantID = plantID;

    }

    public int getPlantID() {
        return plantID;
    }

    public void setPlantID(int plantID) {
        this.plantID = plantID;
    }
}
